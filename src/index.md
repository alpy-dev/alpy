# Alperen "Alp" Yasar

<!-- --- -->

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- 
**Table of Contents** 

- [About me](#about-me)
- [Research](#research)
- [Teaching](#teaching)
- [CV](#cv) -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
<!-- 
## About me -->

I recently received my joint PhD from the Applied Mathematics Department of Paris I Pantheon-Sorbonne University and the Economics Department of the Ca' Foscari University of Venice. As a Marie-Curie PhD fellow within the Horizon2020 project Economic Policy in Complex Environments ([https://epoc-itn.eu][EPOC]), I spent two years in Venice working with Marco LiCalzi and Paolo Pellizzari and one year in Paris working with Michel Grabisch. My dissertation, ``Mental Models: Disruption and Repair,'' focuses on modelling complex beliefs and mental models in social interactions.

I am interested in complex social phenomena that arise from simple human behaviors. I create analytical models that predict biases or fallacies via aggregation functions and/or games, then I use computational tools to study the emergent behavior in the community.

My doctoral education is funded thanks to the European Union's Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No 956107, ["Economic Policy in Complex Environments (EPOC)"](https://epoc-itn.eu/).

Find me at: [LinkedIn](https://www.linkedin.com/in/m-alperen-yasar-78199a2b3/) or [Google Scholar](https://scholar.google.com/citations?user=qVjxDhcAAAAJ&hl=en&oi=ao).

<!-- 
## Research

[Research](https://www.alperenyasar.com/Projects/index.html)

## Teaching

[Teaching](https://www.alperenyasar.com/TEACHING.html)


## CV

[CV](https://www.alperenyasar.com/CV.html) -->

