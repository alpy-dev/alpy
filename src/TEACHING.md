## Tutoring

- Mathematics for social sciences, 2023-2024 Fall.

## Teaching Assistance

During the COVID-19 Pandemic, I assisted the following courses in Bahcesehir University via evaluating their exams:

- Mathematics, 2020-2021 Fall;
- Statistics, 2020-2021 Spring;
- Corporate Finance: 2019-2020 Spring.