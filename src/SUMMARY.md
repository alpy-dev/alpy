# Summary

[Introduction](./index.md)

- [Publications](./Projects/index.md)
    - [First publication](./Projects/HAWKDOVE.md)
    - [Second publication](./Projects/DST.md)
    - [Third publication](./Projects/VA.md)

    <!-- - [Third Chapter](./Projects/INTERVAL.md) -->

[CV](./CV.md)

<!-- - [Teaching](./TEACHING.md) -->
    
