## Vocabulary Aggregation

### Abstract:

 A vocabulary is  a list of words designating subsets of points from a grand set X. We model a vocabulary as a partition of X and study the aggregation of individual vocabularies into a collective one. We characterize aggregation rules when X is linearly ordered and each individual partition is formed by order intervals. Notably, we allow for individual vocabularies to differ both in the number and in the extension of their words.
 
### Working paper:

- LiCalzi, M., & Yasar, M. A. (2024). Vocabulary aggregation. Available in [SSRN](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4902792).