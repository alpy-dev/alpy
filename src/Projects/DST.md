## Frequentist belief update under ambiguous evidence in social networks

### Abstract:

In this paper, we study a frequentist approach to belief updating in the framework of Dempster-Shafer Theory (DST). We propose several mechanisms that allow the gathering of possibly ambiguous pieces of evidence over time to obtain a belief mass assignment. We then use our approach to study the impact of ambiguous evidence on the belief distribution of agents in social networks. We illustrate our approach by taking three representative situations. In the first one, we suppose that there is an unknown state of nature, and agents form belief in the set of possible states. Nature constantly sends a signal which reflects the true state with some probability but which can also be ambiguous. In the second situation, there is no ground truth, and agents are against or in favor of some ethical or societal issues. In the third situation, there is no ground state either, but agents have opinions on left, center, and right political parties. We show that our approach can model various phenomena often observed in social networks, such as polarization or bounded confidence effects.

*Highlights*:

- Creates a frequentist belief update framework for Dempster-Shafer Theory, allowing us a subjective belief system.
- Proposes several update rules for frequentist update methodology and studies their properties. 
- Shows that DST allows for a flexible belief update mechanism in social networks. 
- Studies various settings, including ambiguous evidence, network polarization, and echo chambers under DST.

### Published in:

- Grabisch, M., & Yasar, M. A. (2024). Frequentist Belief Update Under Ambiguous Evidence in Social Networks. _International Journal of Approximate Reasoning, 172_, 109240. [DOI](https://doi.org/10.1016/j.ijar.2024.109240).