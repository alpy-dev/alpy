## Summary

During my dissertation, I studied mental models from different perspectives. 

- See [First Chapter](https://www.alperenyasar.com/Projects/HAWKDOVE.html) for categorization;
- See [Second Chapter](https://www.alperenyasar.com/Projects/DST.html) for belief functions;
- See [Third Chapter](https://www.alperenyasar.com/Projects/VA.html) for vocabulary aggregations.