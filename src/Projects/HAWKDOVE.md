# The emergence of discrimination due to miscategorization

[This code](https://gitlab.com/alpy-dev/hawkdove) is used for the following paper:

### Abstract:

This study explores the emergence of discrimination based on observable characteristics. In many instances, agents presume differences arising from traits such as race or gender, even when these parameters are irrelevant to the situation at hand. This paper intends to reveal an emergent behavior and a persistent culture of discrimination caused by miscategorization in strategic interactions. We assume that agents occasionally engage in conflicts modeled as asymmetric hawk and dove games, where boundedly rational agents may categorize their opponents based on observable traits to make effective decisions. Three categorization strategies are considered: fine-grained, regular, and coarse-grained. Subsequently, an evolutionary agent-based model is employed to examine the performance of these strategies in a dynamic environment. The results demonstrate that fine-grained categorization provides an advantage when the cost of fighting is low, while coarse-grained categorizers exhibit more peaceful behavior, gaining an advantage when the cost of conflict is high. Our primary finding indicates the emergence of discrimination based on non-relevant traits, manifested through consistent aggressive behavior towards individuals possessing these traits. 

### Published in:

- Yasar, Alperen. 2024. "The emergence of discrimination due to miscategorization.” _International Journal of Organization Theory & Behavior_. [DOI](https://doi.org/10.1108/IJOTB-08-2023-0168).