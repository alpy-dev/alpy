# Interval aggregations

My current work focuses on the strategy-proof aggregations of intervals. See:

- Miller, Alan D. "The Limits of Tolerance." (2023).
- Endriss, Ulle, Arianna Novaro, and Zoi Terzopoulou. "Representation Matters: Characterisation and Impossibility Results for Interval Aggregation." IJCAI. 2022.
- Farfel, Joseph, and Vincent Conitzer. "Aggregating value ranges: preference elicitation and truthfulness." Autonomous Agents and Multi-Agent Systems 22.1 (2011): 127-150.

For relevant studies.