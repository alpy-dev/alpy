# CV

## Education

- PhD Student in Applied Mathematics in Paris I Pantheon-Sorbonne University (2021 - Current)
- PhD Student in Economics in Ca' Foscari University of Venice (2021 - Current)
- Galatasaray University: Master's in Economics (2018-2020)

## Publications

- Grabisch, M., & Yasar, M. A. (2024). Frequentist Belief Update Under Ambiguous Evidence in Social Networks. _International Journal of Approximate Reasoning, 172_, 109240. [DOI](https://doi.org/10.1016/j.ijar.2024.109240).
- Yasar, Alperen. 2024. "The emergence of discrimination due to miscategorization.” _International Journal of Organization Theory & Behavior_. [DOI](https://doi.org/10.1108/IJOTB-08-2023-0168).
- LiCalzi, M., & Yasar, M. A. (2024). Vocabulary aggregation. Available in [SSRN](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4902792).

## Conference presentations


The emergence of discrimination in power struggles:

- AMASES XLVI - Annual Meeting of the Italian Association for Mathematics Applied to Social and Economic Sciences; Palermo, Italy; 22.9 – 24.9.2022;
- 17th BiGSEM Doctoral Workshop on Economics and Management; Bielefeld, Germany; 12.12. - 13.12.2022;
- 26th Annual Workshop on Economic Science with Heterogeneous Interacting Agents; Koper, Slovenia; 22.06 - 24.06-2023.


Frequentist belief update under ambiguous evidence in social networks:

- 56th Annual Meeting of the Society for Mathematical Psychology; Amsterdam, Netherlands; 18.07 - 21.07.2023;
- AMASES XLVII - Annual Meeting of the Italian Association for Mathematics Applied to Social and Economic Sciences; Milan, Italy; 20.9 - 22.9.2023.


## Grants

- Marie Skłodowska-Curie Fellowship: My doctoral education has received funding from the European Union’s Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No 956107, see [EPOC](epoc-itn.eu) for more information.

## Teaching

- Mathematics for social sciences, 2023-2024 Fall.
